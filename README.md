# Semantic Annotation Platform with Provenance

CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Maintainers
 
INTRODUCTION
------------

SAPP is a Semantic Annotation Platform with Provenance and is designed on the basis of Semantic Web.
The platform or its modules allows you to annotate genomes of various qualities with the full chain of provenance.
Resulting is a RDF genome data model which you can query and analyse using SPARQL.
Various modules are available which allows you to annotate, visualize and export to various formats.

 * For a full description of each module, visit the project page:
   https://gitlab.com/sapp/

 * To submit bug reports and feature suggestions, or to track changes:
   https://gitlab.com/sapp
   
REQUIREMENTS
------------

SAPP is depending on various modules which will be installed when you follow the installation procedure via install.sh

INSTALLATION
------------

 * Most of the SAPP modules can be installed individually using MAVEN or through the provided ./install.sh file inside each module.
 
 * The SAPP main module contains a galaxy config file allowing you to use the modules inside a clean Galaxy instance as configuration files are currently overwritten.
 
 * To install the complete package: clone this module inside the tools directory of Galaxy.

. 

    git clone https://gitlab.com/sapp/sapp.git


 Note: When using this installation script, a postgresql database is created for Galaxy and extra dependencies are installed which requires root privilages.
 
 To start the installation procedure: ./install.sh


    cd sapp
    sudo ./install.sh
 
 
 
 
 
 
 
MAINTAINERS
-----------

Current maintainers:
 * Jasper Koehorst
 * Jesse van Dam