#!/bin/bash
#The instalation script for all Downloads

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

echo "Do you wish to install all dependencies automatically (admin rights needed)? We also have a virtual machine available of ~10Gb..." yn
select yn in "Yes" "No"; do
	case $yn in
		Yes )

		if [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
			#Dependency to run screens in a screensession
			sudo apt-get install xvfb 
			#Python 3.4
			sudo apt-get install python3.4
			sudo apt-get install python3.4-dev
			####################################################
			#ORACLE Database setup... Only if galaxy is present
			if [ -f ../../config/galaxy.ini ]; then
				sudo apt-get install postgresql postgresql-contrib
				echo "CREATE USER galaxy WITH PASSWORD 'galaxy1234'" | sudo -u postgres psql postgres
				sudo -u postgres createdb --owner=galaxy galaxydb
			fi
			####################################################
			#MAVEN
			sudo apt-get install maven
			####################################################
			#Rjava stuff
			sudo apt-get -y build-dep libcurl4-gnutls-dev
			sudo apt-get -y install libcurl4-gnutls-dev
			sudo apt-get install r-cran-rjava
			sudo apt-get install libgd-perl
		elif [ "$(uname)" == "Darwin" ]; then
			####################################################
			# Installation of homebrew
			ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
			brew update
			brew install maven
			brew install wget
			####################################################
			#ORACLE Database setup... Only if galaxy is present
			if [ -f ../../config/galaxy.ini ]; then
				brew install postgresql
				ln -sfv /usr/local/opt/postgresql/*.plist ~/Library/LaunchAgents
				#There is a tmp connection socket issue sometimes
				launchctl unload ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist
				launchctl load ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist
				####
				# initdb /usr/local/var/postgres -E utf8
				me="$(whoami)"
				echo "CREATE USER galaxy WITH PASSWORD 'galaxy1234'" | sudo -u $me psql postgres
				sudo -u $me createdb --owner=galaxy galaxydb
			fi

		else
			echo I detected a non-unix system...
			break
		fi

		if [ -f ../../config/galaxy.ini ]; then
			#Galaxy configure scripts
			cp ./settings/tool_conf.xml ../../config/
			cp ./settings/SAPP.yaml ../../config/plugins/tours/
			#IF file already exists skip
		fi

		if [ -f ../../config/galaxy.ini.sample ]; then
			#Run replace command tobereplaced@email.com
			echo "Type in the email adress that will be used for galaxy admin user:"
			read emailGalaxy
			sed -e s/tobereplaced@email.com/$emailGalaxy/g ./settings/galaxy.ini.sample > ../../config/galaxy.ini
			cp -r settings/sapp/ ../../static/
		fi

		####################################################
		# #Installation of Forester
		if [ ! -f ./Programs/forester_1038.jar ]; then
			wget -nc https://googledrive.com/host/0BxMokdxOh-JRM1d2azFoRnF3bGM/download/forester_1038.jar -P ./Programs
		fi
		# ####################################################
		#Python modules
		wget -nc https://bootstrap.pypa.io/ez_setup.py -P ./
		sudo python3.4 ./ez_setup.py
		rm ./ez_setup.py
		sudo easy_install-3.4 pip

		#Python modules - Numpy
		sudo pip3.4 install numpy

		#Python modules - Biopython
		sudo pip3.4 install biopython

		#Python modules - RDFLIB
		sudo pip3.4 install rdflib

		# ####################################################
		#Perl - CPAN
		install CPAN
	    reload cpan		
		####################################################
		#R dependencies
		sudo R CMD javareconf
		Rscript install.R
		break;;

	No ) echo "No!"
	esac
done


#Inititalising all...

git pull &&
git submodule init &&
git submodule update &&
git submodule foreach git checkout master &&

#####################################################

git submodule foreach git pull