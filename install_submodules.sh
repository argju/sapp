if grep -q ^$path $toplevel/modules_install; 
then 
    if grep -q `git rev-parse HEAD` $toplevel/modules_skip_commits; 
    then 
	: 
    else 
	if [ -f "./install.sh" ]; 
	then 
	    echo "** Trying to install module $path"
	    ./install.sh && 
	    echo "$path `git rev-parse HEAD`" >> $toplevel/modules_skip_commits ||
	    ( 	echo "Failed to install $path"
		echo "$path `git rev-parse HEAD`" >> $toplevel/modules_failed 
		sort -u $toplevel/modules_failed > $toplevel/tmp_failed 
		mv $toplevel/tmp_failed $toplevel/modules_failed 
	    ) 
	fi 
    fi
fi

