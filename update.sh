#!/bin/bash
# Exists to fully update the git repo that you are sitting in...

git pull &&
git submodule init &&
git submodule update &&
git submodule foreach git checkout master &&

#####################################################

git submodule foreach git pull 

echo "*** Skipping the following modules that are commented out or missing in $(pwd)/modules_install"
if [ ! -f modules_install ]; then (echo "#Comment out to skip modules"; git submodule --quiet foreach 'echo $path') >> modules_install; fi   #default : install all submodules
git submodule --quiet foreach 'if grep -q ^$path $toplevel/modules_install; then :; else echo $path; fi'

echo "*** Skipping the following modules where HEAD is in $(pwd)/modules_skip_commits"
if [ ! -f modules_skip_commits ]; then echo "#Commits that will not be installed again" > modules_skip_commits; fi #default : skip none
git submodule --quiet foreach 'if grep -q ^$path $toplevel/modules_install; then if grep -q `git rev-parse HEAD` $toplevel/modules_skip_commits; then echo $path; fi; fi'

echo "*** Installing remaining modules"
mkdir -p modules_logs
git submodule --quiet foreach '. $toplevel/install_submodules.sh |& tee $toplevel/modules_logs/`echo $name | sed s/"\/"/"_"/g`.log '

if [ -f ../../config/tool_conf.xml ]; then
    cp ./settings/tool_conf.xml ../../config/tool_conf.xml
fi
